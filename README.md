# Indicium Airflow Hands-on Tutorial

Resolução do DesafioAirflow feito por Marcos Vinicios Ferreira Rosa
## O Problema

Precisamos extrair os dados do banco northwind (banco demo de ecommerce) para hd local primeiro, e depois do hd para um segundo banco de dados. Também precisamos fazer load de um arquivo com informação de vendas que por algum motivo vem de outro sistema em um arquivo csv. 

Com esses dados juntos em um database, gostariamos de saber quanto foi vendido em um dia.